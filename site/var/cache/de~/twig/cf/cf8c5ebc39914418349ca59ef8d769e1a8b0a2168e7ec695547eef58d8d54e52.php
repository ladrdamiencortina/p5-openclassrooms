<?php

/* default/characterSelect.html.twig */
class __TwigTemplate_f5b3483cdf40ab95942777bcbeec353ff4d2a216f04b071f62d717b8adb406fb extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 3
        $this->parent = $this->loadTemplate("base.html.twig", "default/characterSelect.html.twig", 3);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/characterSelect.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/characterSelect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 6
        echo "  ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
  <style type=\"text/css\">
    .selected {
      border: 3px solid white;
      border-radius: 5px;
    }
  </style>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 15
    public function block_head($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 16
        echo "    <h1 class=\"title\">
      Choix de personnage
    </h1>
    <h2 class=\"subtitle\">
      Avant de vous lancer... Choississez le personnage que vous allez incarner !
    </h2>
    ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 22, $this->source); })()), "flashes", array()));
        foreach ($context['_seq'] as $context["label"] => $context["messages"]) {
            // line 23
            echo "              ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["messages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 24
                echo "                  <div class=\"alert alert-";
                echo twig_escape_filter($this->env, $context["label"], "html", null, true);
                echo "\" style=\"margin-top:40px;\">
                      ";
                // line 25
                echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                echo "
                  </div>
              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 28
            echo "          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['label'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 31
    public function block_menu($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 32
        echo "    <li><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage");
        echo "\">Accueil</a></li>
    <li class=\"is-active\"><a>Personnages</a></li>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 36
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 37
        echo "<div class=\"container\" style=\"margin-top:25px;\">

<div class=\"columns\">
  ";
        // line 40
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["characters"]) || array_key_exists("characters", $context) ? $context["characters"] : (function () { throw new Twig_Error_Runtime('Variable "characters" does not exist.', 40, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["character"]) {
            // line 41
            echo "  <div class=\"column\">
  <div class=\"card\" style=\"height:100%; position:relative\">
  <div class=\"card-image\" style=\"position:relative;\">
    <figure class=\"image is-4by3\">
      <img class=\"pixel absolute-center\" src=\"";
            // line 45
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("assets/textures/player/" . twig_get_attribute($this->env, $this->source, $context["character"], "image", array()))), "html", null, true);
            echo "\" alt=\"Placeholder image\" style=\"width:75%; height:75%;\">
    </figure>
  </div>
  <div class=\"card-content\" style=\"margin-bottom:25px;\">
    <div class=\"media\">
      <div class=\"media-content\">
        <p class=\"title is-4 has-text-centered\">";
            // line 51
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["character"], "name", array()), "html", null, true);
            echo "</p>
      </div>
    </div>

    <div class=\"content\">
      <tables class=\"table is-bordered is-striped is-narrow is-hoverable is-fullwidth\">

      </table>
      <a class=\"panel-block\">
        <span class=\"panel-icon\">
          <i class=\"fas fa-fist-raised\" aria-hidden=\"true\"></i>
        </span>
        Arme : ";
            // line 63
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["character"], "defaultWeapon", array()), "name", array()), "html", null, true);
            echo "
      </a>
    </table>
    <a class=\"panel-block\">
      <span class=\"panel-icon\">
        <i class=\"fas fa-haykal\" aria-hidden=\"true\"></i>
      </span>
      Dégats : ";
            // line 70
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["character"], "defaultWeapon", array()), "dammage", array()), "html", null, true);
            echo "
    </a>
  </table>
  <a class=\"panel-block\">
    <span class=\"panel-icon\">
      <i class=\"fas fa-wind\" aria-hidden=\"true\"></i>
    </span>
    Vitesse d'attaque : ";
            // line 77
            echo twig_escape_filter($this->env, (5 - (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["character"], "defaultWeapon", array()), "fireRate", array()) / 1000)), "html", null, true);
            echo "
  </a>
  <a class=\"panel-block\">
    <span class=\"panel-icon\">
      <i class=\"fas fa-shoe-prints\" aria-hidden=\"true\"></i>
    </span>
    Vitesse de déplacement : ";
            // line 83
            echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, $context["character"], "speed", array()) * 10), "html", null, true);
            echo "
  </a>
      <p class=\"box\" style=\"margin-top:10px;\">";
            // line 85
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["character"], "description", array()), "html", null, true);
            echo "</p>
    </div>
    <div class=\"has-text-centered\" style=\" position:absolute; bottom:25px; left:0px; width:100%; color:#fff\">
      ";
            // line 88
            if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 88, $this->source); })()), "user", array()), "character", array()) != null) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 88, $this->source); })()), "user", array()), "character", array()), "id", array()) == twig_get_attribute($this->env, $this->source, $context["character"], "id", array())))) {
                // line 89
                echo "        <a class=\"button is-primary character is-outlined\" data-character=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["character"], "id", array()), "html", null, true);
                echo "\">Sélectionné</a>
      ";
            } else {
                // line 91
                echo "        <a class=\"button is-primary character\" data-character=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["character"], "id", array()), "html", null, true);
                echo "\">Choisir</a>
      ";
            }
            // line 93
            echo "  </div>
  </div>
</div>
</div>

  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['character'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 99
        echo "  </div>
</div>
  <footer class=\"footer\" style=\"margin-top:1.5rem;\">
      <div class=\"container\">
          <div class=\"content has-text-centered\">
              <p>
                  <a class=\"icon\" href=\"https://github.com/dansup/bulma-templates\">
                      <i class=\"fas fa-gamepad has-text-primary\"></i>
                  </a>
              </p>
              <div class=\"control level-item\">
                  <a href=\"https://github.com/dansup/bulma-templates\">
                      <div class=\"tags has-addons\">
                          <span class=\"tag is-dark\">Dev & Dungeon</span>
                          <span class=\"tag is-primary\">Damien Cortina</span>
                      </div>
                  </a>
              </div>
          </div>
      </div>
  </footer>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 122
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 123
        echo "  ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
  <script type=\"text/javascript\">
    \$('.character').on('click', function() {
      \$(this).addClass('is-loading');
      var that = this;
      \$.ajax({
        type: 'POST',
        url: \"/app_dev.php/characterSelect\",
        data: {
          character: \$(that).data('character')
        }
      }).done(function(data) {
        \$('.character').removeClass('is-outlined');
        \$('.character').html('Choisir');
        \$(that).removeClass('is-loading');
        \$(that).addClass('is-outlined');
        \$(that).html('Sélectionné');
      }).fail(function(jqXHR, textStatus, errorThrown) {
        if (typeof jqXHR.responseJSON !== 'undefined') {
          alert(errorThrown);
        } else {
          alert(errorThrown);
        }
      });
    })
  </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "default/characterSelect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  306 => 123,  297 => 122,  266 => 99,  255 => 93,  249 => 91,  243 => 89,  241 => 88,  235 => 85,  230 => 83,  221 => 77,  211 => 70,  201 => 63,  186 => 51,  177 => 45,  171 => 41,  167 => 40,  162 => 37,  153 => 36,  139 => 32,  130 => 31,  116 => 28,  107 => 25,  102 => 24,  97 => 23,  93 => 22,  85 => 16,  76 => 15,  57 => 6,  48 => 5,  15 => 3,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# src/DC/UserBundle/Resources/views/Security/login.html.twig #}

{% extends \"base.html.twig\" %}

{% block stylesheets %}
  {{parent()}}
  <style type=\"text/css\">
    .selected {
      border: 3px solid white;
      border-radius: 5px;
    }
  </style>
{% endblock %}

{% block head %}
    <h1 class=\"title\">
      Choix de personnage
    </h1>
    <h2 class=\"subtitle\">
      Avant de vous lancer... Choississez le personnage que vous allez incarner !
    </h2>
    {% for label, messages in app.flashes %}
              {% for message in messages %}
                  <div class=\"alert alert-{{ label }}\" style=\"margin-top:40px;\">
                      {{ message }}
                  </div>
              {% endfor %}
          {% endfor %}
{% endblock %}

{% block menu %}
    <li><a href=\"{{path('homepage')}}\">Accueil</a></li>
    <li class=\"is-active\"><a>Personnages</a></li>
{% endblock %}

{% block body %}
<div class=\"container\" style=\"margin-top:25px;\">

<div class=\"columns\">
  {% for character in characters %}
  <div class=\"column\">
  <div class=\"card\" style=\"height:100%; position:relative\">
  <div class=\"card-image\" style=\"position:relative;\">
    <figure class=\"image is-4by3\">
      <img class=\"pixel absolute-center\" src=\"{{asset(\"assets/textures/player/\"~character.image)}}\" alt=\"Placeholder image\" style=\"width:75%; height:75%;\">
    </figure>
  </div>
  <div class=\"card-content\" style=\"margin-bottom:25px;\">
    <div class=\"media\">
      <div class=\"media-content\">
        <p class=\"title is-4 has-text-centered\">{{character.name}}</p>
      </div>
    </div>

    <div class=\"content\">
      <tables class=\"table is-bordered is-striped is-narrow is-hoverable is-fullwidth\">

      </table>
      <a class=\"panel-block\">
        <span class=\"panel-icon\">
          <i class=\"fas fa-fist-raised\" aria-hidden=\"true\"></i>
        </span>
        Arme : {{character.defaultWeapon.name}}
      </a>
    </table>
    <a class=\"panel-block\">
      <span class=\"panel-icon\">
        <i class=\"fas fa-haykal\" aria-hidden=\"true\"></i>
      </span>
      Dégats : {{character.defaultWeapon.dammage}}
    </a>
  </table>
  <a class=\"panel-block\">
    <span class=\"panel-icon\">
      <i class=\"fas fa-wind\" aria-hidden=\"true\"></i>
    </span>
    Vitesse d'attaque : {{5 - character.defaultWeapon.fireRate / 1000}}
  </a>
  <a class=\"panel-block\">
    <span class=\"panel-icon\">
      <i class=\"fas fa-shoe-prints\" aria-hidden=\"true\"></i>
    </span>
    Vitesse de déplacement : {{character.speed * 10}}
  </a>
      <p class=\"box\" style=\"margin-top:10px;\">{{character.description}}</p>
    </div>
    <div class=\"has-text-centered\" style=\" position:absolute; bottom:25px; left:0px; width:100%; color:#fff\">
      {% if app.user.character != null and app.user.character.id == character.id %}
        <a class=\"button is-primary character is-outlined\" data-character=\"{{character.id}}\">Sélectionné</a>
      {% else %}
        <a class=\"button is-primary character\" data-character=\"{{character.id}}\">Choisir</a>
      {% endif %}
  </div>
  </div>
</div>
</div>

  {% endfor %}
  </div>
</div>
  <footer class=\"footer\" style=\"margin-top:1.5rem;\">
      <div class=\"container\">
          <div class=\"content has-text-centered\">
              <p>
                  <a class=\"icon\" href=\"https://github.com/dansup/bulma-templates\">
                      <i class=\"fas fa-gamepad has-text-primary\"></i>
                  </a>
              </p>
              <div class=\"control level-item\">
                  <a href=\"https://github.com/dansup/bulma-templates\">
                      <div class=\"tags has-addons\">
                          <span class=\"tag is-dark\">Dev & Dungeon</span>
                          <span class=\"tag is-primary\">Damien Cortina</span>
                      </div>
                  </a>
              </div>
          </div>
      </div>
  </footer>
{% endblock %}

{% block javascripts %}
  {{parent()}}
  <script type=\"text/javascript\">
    \$('.character').on('click', function() {
      \$(this).addClass('is-loading');
      var that = this;
      \$.ajax({
        type: 'POST',
        url: \"/app_dev.php/characterSelect\",
        data: {
          character: \$(that).data('character')
        }
      }).done(function(data) {
        \$('.character').removeClass('is-outlined');
        \$('.character').html('Choisir');
        \$(that).removeClass('is-loading');
        \$(that).addClass('is-outlined');
        \$(that).html('Sélectionné');
      }).fail(function(jqXHR, textStatus, errorThrown) {
        if (typeof jqXHR.responseJSON !== 'undefined') {
          alert(errorThrown);
        } else {
          alert(errorThrown);
        }
      });
    })
  </script>
{% endblock javascripts %}
", "default/characterSelect.html.twig", "/var/www/megaman-3d/app/Resources/views/default/characterSelect.html.twig");
    }
}
