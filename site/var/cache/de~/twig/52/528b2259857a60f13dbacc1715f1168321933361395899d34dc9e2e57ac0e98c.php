<?php

/* default/game.html.twig */
class __TwigTemplate_3d139e6c00cfca2d54605adf5745df20b8898cf769c9b03a57c90a7d0f030f56 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/game.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/game.html.twig"));

        // line 1
        echo "<!DOCTYPE HTML>
<html>
  <head>
    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0\">
    <title>Megaman3D</title>
    <meta name=\"description\" content=\"READY!\">
    <!-- SCRIPTS DE BASE -->
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/babylonjs/3.3.0/babylon.js\"></script>
    <script
\t\t\t  src=\"https://code.jquery.com/jquery-3.3.1.min.js\"
\t\t\t  integrity=\"sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=\"
\t\t\t  crossorigin=\"anonymous\"></script>
        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin=\"anonymous\">

    <!-- FEUILLES CSS -->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/stylesheets/gameStyle.css"), "html", null, true);
        echo "\" />
  </head>
  <body>
    ";
        // line 21
        echo "      ";
        // line 22
        echo "    ";
        // line 23
        echo "      <canvas id=\"canvas\"></canvas>
      <div id=\"messagePanel\" class=\"panel panel-default absolute-center\" style=\"top:-50%; background-color:white; padding:20px; border-radius:3px;\">
          <div class=\"panel-body\">
            <h4 class=\"text-center\" style=\"margin:0px;\"><strong id=\"status\">CLASSEMENT</strong></h4>
            <table class=\"table\">
  <thead>
    <tr>
      <th scope=\"col\">Nom</th>
      <th scope=\"col\">Kills</th>
      <th scope=\"col\">Score</th>
    </tr>
  </thead>
  <tbody id=\"player_table\">
  </tbody>
</table>
          </div>
        </div>
  </body>
  <script src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/fosjsrouting/js/router.min.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 42
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("fos_js_routing_js", array("callback" => "fos.Router.setData"));
        echo "\"></script>
  <!-- SCRIPTS BABYLON -->
  <script src=\"https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.0/socket.io.js\"></script>
  <script src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/game/NetworkManager.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/game/Game.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/game/Player.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/game/Arena.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/game/Weapons.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/game/GhostPlayer.js"), "html", null, true);
        echo "\"></script>


</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "default/game.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 50,  103 => 49,  99 => 48,  95 => 47,  91 => 46,  87 => 45,  81 => 42,  77 => 41,  57 => 23,  55 => 22,  53 => 21,  47 => 17,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE HTML>
<html>
  <head>
    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0\">
    <title>Megaman3D</title>
    <meta name=\"description\" content=\"READY!\">
    <!-- SCRIPTS DE BASE -->
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/babylonjs/3.3.0/babylon.js\"></script>
    <script
\t\t\t  src=\"https://code.jquery.com/jquery-3.3.1.min.js\"
\t\t\t  integrity=\"sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=\"
\t\t\t  crossorigin=\"anonymous\"></script>
        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin=\"anonymous\">

    <!-- FEUILLES CSS -->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"{{asset('assets/stylesheets/gameStyle.css')}}\" />
  </head>
  <body>
    {#<audio id=\"music\" loop>#}
      {#<source src=\"{{asset('assets/audio/musics/QUIMUS.ogg')}}\" type=\"audio/ogg\" />#}
    {#</audio>#}
      <canvas id=\"canvas\"></canvas>
      <div id=\"messagePanel\" class=\"panel panel-default absolute-center\" style=\"top:-50%; background-color:white; padding:20px; border-radius:3px;\">
          <div class=\"panel-body\">
            <h4 class=\"text-center\" style=\"margin:0px;\"><strong id=\"status\">CLASSEMENT</strong></h4>
            <table class=\"table\">
  <thead>
    <tr>
      <th scope=\"col\">Nom</th>
      <th scope=\"col\">Kills</th>
      <th scope=\"col\">Score</th>
    </tr>
  </thead>
  <tbody id=\"player_table\">
  </tbody>
</table>
          </div>
        </div>
  </body>
  <script src=\"{{ asset('bundles/fosjsrouting/js/router.min.js') }}\"></script>
  <script src=\"{{ path('fos_js_routing_js', { callback: 'fos.Router.setData' }) }}\"></script>
  <!-- SCRIPTS BABYLON -->
  <script src=\"https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.0/socket.io.js\"></script>
  <script src=\"{{asset('assets/js/game/NetworkManager.js')}}\"></script>
  <script src=\"{{asset('assets/js/game/Game.js')}}\"></script>
  <script src=\"{{asset('assets/js/game/Player.js')}}\"></script>
  <script src=\"{{asset('assets/js/game/Arena.js')}}\"></script>
  <script src=\"{{asset('assets/js/game/Weapons.js')}}\"></script>
  <script src=\"{{asset('assets/js/game/GhostPlayer.js')}}\"></script>


</html>
", "default/game.html.twig", "/var/www/megaman-3d/app/Resources/views/default/game.html.twig");
    }
}
