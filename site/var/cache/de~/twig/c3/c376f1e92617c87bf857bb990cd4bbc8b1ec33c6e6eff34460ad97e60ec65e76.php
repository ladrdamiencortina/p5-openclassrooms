<?php

/* @DCUser/Security/logddin.html.twig */
class __TwigTemplate_43aa05dd8b8ad92dabe478460c326f10d2d0fc9681cd5978a0ab50ec2c7a701f extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 3
        $this->parent = $this->loadTemplate("base.html.twig", "@DCUser/Security/logddin.html.twig", 3);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@DCUser/Security/logddin.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@DCUser/Security/logddin.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 6
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<style>
input[type=text], input[type=password] {
    background: transparent;
    outline : none;
    border : none;
    border-bottom : 2px solid white;
    border-radius : 0px;
    border-image : linear-gradient(to right,#007ec6 0%,#d71418 51%, #007ec6 100%) 1;
    color: white;
    font-weight: bold;
}
input[type=text]:focus, input[type=password]:focus {
    background : transparent;
    outline : none;
    border-bottom: 2px solid white;
    border-color:#007ec6;
    box-shadow: none;
    color:white;
}

input[type=text] + i, input[type=password] + i {
  color :white;
  transition: color 0.2s ease-in-out;
}

input[type=text]:focus + i, input[type=password]:focus + i {
  color : #007ec6;
}

.labelled-input {
  padding:0px 0px 0px 50px !important:
}

.btn-grad {
  position: relative;
  transition: background-color 0.4s ease-in-out;
  border-radius:25px;
  background-color: black;
  width:100%;
  height:50px;
  font-weight:bold;
  margin-top:30px;
  color:white;
}

.btn-grad:hover {
   background-color: transparent;
 }

 .btn-grad::after {
    position: absolute;
    top: -3px; bottom: -3px;
    left: -3px; right: -3px;
    background-image: linear-gradient(to right, #007ec6 0%,#d71418 51%, #007ec6 100%);
    content: '';
    z-index: -1;
    border-radius: 25px;
}

form {
  padding : 0px 20px 50px 20px;
}

.login-logo {
  width:400px;
}

.field {
  margin-top:25px;
  position: relative;
}

.form-fa {
  height:0px;
  position: absolute;
  bottom : 30px;
  left:25px;
}

</style>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 89
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 90
        echo "<div class=\"stary\">
  <div id='stars'></div>
  <div id='stars2'></div>
  <div id='stars3'></div>
  <img src=\"";
        // line 94
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/img/megaman_login _emptied.png"), "html", null, true);
        echo "\" style=\"position:absolute; width:100%; height:100vh;\">
    <div class=\"container\" style=\"height:95vh;display:flex;align-items: center;justify-content: center; \">
  ";
        // line 97
        echo "
<div class=\"row\">


  ";
        // line 102
        echo "  <form action=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("login_check");
        echo "\" method=\"post\" style=\"border:1px solid grey; border-radius:5px; background-color:rgba(0,0,0,0.3); color:white; z-index:100\">
    <img class=\"login-logo\" src=\"";
        // line 103
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/img/megaman_logo.png"), "html", null, true);
        echo "\">
    ";
        // line 104
        if ((isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 104, $this->source); })())) {
            // line 105
            echo "      <div class=\"alert alert-danger\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 105, $this->source); })()), "message", array()), "html", null, true);
            echo "</div>
    ";
        }
        // line 107
        echo "    <div class=\"col-md-12 field\">
      <label for=\"username\">Identifiant</label>
      <input type=\"text\" id=\"username\" name=\"_username\" value=\"";
        // line 109
        echo twig_escape_filter($this->env, (isset($context["last_username"]) || array_key_exists("last_username", $context) ? $context["last_username"] : (function () { throw new Twig_Error_Runtime('Variable "last_username" does not exist.', 109, $this->source); })()), "html", null, true);
        echo "\" class=\"form-control labelled-input\" style=\"padding-left : 40px\" placeholder=\"Entrez votre identifiant\"/>
      <i class=\"fa fa-user form-fa\"></i>
  </div>
  <div class=\"col-md-12 field\">
    <label for=\"password\">Mot de passe</label>
    <input type=\"password\" id=\"password\" name=\"_password\" class=\"form-control\" style=\"padding-left : 40px\" placeholder=\"Entrez votre mot de passe\"/>
    <i class=\"fa fa-lock form-fa\"></i>
    </div>
    <br />
    <div class=\"col-md-12 text-center\">
      <button type=\"submit\" class=\"btn btn-grad\">Connexion</button>
  </div>
  </form>
</div>
</div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@DCUser/Security/logddin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  193 => 109,  189 => 107,  183 => 105,  181 => 104,  177 => 103,  172 => 102,  166 => 97,  161 => 94,  155 => 90,  146 => 89,  54 => 6,  45 => 5,  15 => 3,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# src/DC/UserBundle/Resources/views/Security/login.html.twig #}

{% extends \"base.html.twig\" %}

{% block stylesheets %}
{{parent()}}
<style>
input[type=text], input[type=password] {
    background: transparent;
    outline : none;
    border : none;
    border-bottom : 2px solid white;
    border-radius : 0px;
    border-image : linear-gradient(to right,#007ec6 0%,#d71418 51%, #007ec6 100%) 1;
    color: white;
    font-weight: bold;
}
input[type=text]:focus, input[type=password]:focus {
    background : transparent;
    outline : none;
    border-bottom: 2px solid white;
    border-color:#007ec6;
    box-shadow: none;
    color:white;
}

input[type=text] + i, input[type=password] + i {
  color :white;
  transition: color 0.2s ease-in-out;
}

input[type=text]:focus + i, input[type=password]:focus + i {
  color : #007ec6;
}

.labelled-input {
  padding:0px 0px 0px 50px !important:
}

.btn-grad {
  position: relative;
  transition: background-color 0.4s ease-in-out;
  border-radius:25px;
  background-color: black;
  width:100%;
  height:50px;
  font-weight:bold;
  margin-top:30px;
  color:white;
}

.btn-grad:hover {
   background-color: transparent;
 }

 .btn-grad::after {
    position: absolute;
    top: -3px; bottom: -3px;
    left: -3px; right: -3px;
    background-image: linear-gradient(to right, #007ec6 0%,#d71418 51%, #007ec6 100%);
    content: '';
    z-index: -1;
    border-radius: 25px;
}

form {
  padding : 0px 20px 50px 20px;
}

.login-logo {
  width:400px;
}

.field {
  margin-top:25px;
  position: relative;
}

.form-fa {
  height:0px;
  position: absolute;
  bottom : 30px;
  left:25px;
}

</style>
{% endblock %}

{% block body %}
<div class=\"stary\">
  <div id='stars'></div>
  <div id='stars2'></div>
  <div id='stars3'></div>
  <img src=\"{{asset('assets/img/megaman_login _emptied.png')}}\" style=\"position:absolute; width:100%; height:100vh;\">
    <div class=\"container\" style=\"height:95vh;display:flex;align-items: center;justify-content: center; \">
  {# S'il y a une erreur, on l'affiche dans un joli cadre #}

<div class=\"row\">


  {# Le formulaire, avec URL de soumission vers la route « login_check » comme on l'a vu #}
  <form action=\"{{ path('login_check') }}\" method=\"post\" style=\"border:1px solid grey; border-radius:5px; background-color:rgba(0,0,0,0.3); color:white; z-index:100\">
    <img class=\"login-logo\" src=\"{{asset('assets/img/megaman_logo.png')}}\">
    {% if error %}
      <div class=\"alert alert-danger\">{{ error.message }}</div>
    {% endif %}
    <div class=\"col-md-12 field\">
      <label for=\"username\">Identifiant</label>
      <input type=\"text\" id=\"username\" name=\"_username\" value=\"{{ last_username }}\" class=\"form-control labelled-input\" style=\"padding-left : 40px\" placeholder=\"Entrez votre identifiant\"/>
      <i class=\"fa fa-user form-fa\"></i>
  </div>
  <div class=\"col-md-12 field\">
    <label for=\"password\">Mot de passe</label>
    <input type=\"password\" id=\"password\" name=\"_password\" class=\"form-control\" style=\"padding-left : 40px\" placeholder=\"Entrez votre mot de passe\"/>
    <i class=\"fa fa-lock form-fa\"></i>
    </div>
    <br />
    <div class=\"col-md-12 text-center\">
      <button type=\"submit\" class=\"btn btn-grad\">Connexion</button>
  </div>
  </form>
</div>
</div>
</div>
{% endblock %}
", "@DCUser/Security/logddin.html.twig", "/var/www/megaman-3d/src/DC/UserBundle/Resources/views/Security/logddin.html.twig");
    }
}
