<?php

/* default/homepage.html.twig */
class __TwigTemplate_532dbecaa196da3991445a3602ea32d092fd91341959f3c5f0d4d4995354f026 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 3
        $this->parent = $this->loadTemplate("base.html.twig", "default/homepage.html.twig", 3);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/homepage.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/homepage.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_head($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 6
        echo "    <h1 class=\"title\">
      Bienvenue sur Dev & Dungeon
    </h1>
    <h2 class=\"subtitle\">
      Dev & Dungeon est un jeu de rôle à la première personne dont le but est de défaire vos adversaire pour gagner de points,<br/> et ainsi remporter la partie...
    </h2>
    <div class=\"has-text-centered\"><a href=\"";
        // line 12
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("game");
        echo "\" class=\"button is-primary is-inverted is-outlined is-medium\">Jouer</a></div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 15
    public function block_menu($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 16
        echo "    <li class=\"is-active\"><a>Accueil</a></li>
    <li><a href=\"";
        // line 17
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("character_select");
        echo "\">Personnages</a></li>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 20
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 21
        echo "
<section class=\"container\">
  <div class=\"box cta\" style=\"margin-top:1.5rem;\">
              <p class=\"has-text-centered\">
                  <span class=\"tag is-primary\">Nouveau</span> La première version du jeu est enfin disponible !
              </p>
  </div>
            <div class=\"columns features\" style=\"margin-top:1.5rem;\">
                <div class=\"column is-4\">
                    <div class=\"card is-shady\" style=\"padding:15px;\">
                        <div class=\"card-image has-text-centered\">
                            <i class=\"fa fa-user fa-5x\"></i>
                        </div>
                        <div class=\"card-content\">
                            <div class=\"content\">
                                <h4>Choisissez un personnage </h4>
                                <p>Chacun à des caractéristiques disctinctes, et par conséquent ses points forts et faiblesses... A vous de les exploiter au mieux</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=\"column is-4\">
                    <div class=\"card is-shady\" style=\"padding:15px;\">
                        <div class=\"card-image has-text-centered\">
                          <i class=\"fas fa-users fa-5x\"></i>
                        </div>
                        <div class=\"card-content\">
                            <div class=\"content\">
                                <h4>Combattez vos ennemis </h4>
                                <p>Combattez vos ennemis (et amis)! N'oubliez pas d'admirer le sympatique environnement pixélisé entre 2 coups d'épée !</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=\"column is-4\">
                    <div class=\"card is-shady\" style=\"padding:15px;\">
                        <div class=\"card-image has-text-centered\">
                            <i class=\"fas fa-trophy fa fa-5x\"></i>
                        </div>
                        <div class=\"card-content\">
                            <div class=\"content\">
                                <h4>Tentez de remporter la victoire </h4>
                                <p>Soyez le plus vifs pour dominer la partie ! Celui qui obtient le plus de points l'emporte ! Mais n'oubliez pas que ce n'est qu'un jeu...</p>
                            </div>
                        </div>
                    </div>
                </div>
</div>
</section>
<footer class=\"footer\" style=\"margin-top:1.5rem;\">
    <div class=\"container\">
        <div class=\"content has-text-centered\">
            <p>
                <a class=\"icon\" href=\"https://github.com/dansup/bulma-templates\">
                    <i class=\"fas fa-gamepad has-text-primary\"></i>
                </a>
            </p>
            <div class=\"control level-item\">
                <a href=\"https://github.com/dansup/bulma-templates\">
                    <div class=\"tags has-addons\">
                        <span class=\"tag is-dark\">Dev & Dungeon</span>
                        <span class=\"tag is-primary\">Damien Cortina</span>
                    </div>
                </a>
            </div>
        </div>
    </div>
</footer>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 91
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 92
        echo "
";
        // line 93
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "default/homepage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  199 => 93,  196 => 92,  187 => 91,  109 => 21,  100 => 20,  88 => 17,  85 => 16,  76 => 15,  64 => 12,  56 => 6,  47 => 5,  15 => 3,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# src/DC/UserBundle/Resources/views/Security/login.html.twig #}

{% extends \"base.html.twig\" %}

{% block head %}
    <h1 class=\"title\">
      Bienvenue sur Dev & Dungeon
    </h1>
    <h2 class=\"subtitle\">
      Dev & Dungeon est un jeu de rôle à la première personne dont le but est de défaire vos adversaire pour gagner de points,<br/> et ainsi remporter la partie...
    </h2>
    <div class=\"has-text-centered\"><a href=\"{{path('game')}}\" class=\"button is-primary is-inverted is-outlined is-medium\">Jouer</a></div>
{% endblock %}

{% block menu %}
    <li class=\"is-active\"><a>Accueil</a></li>
    <li><a href=\"{{path('character_select')}}\">Personnages</a></li>
{% endblock %}

{% block body %}

<section class=\"container\">
  <div class=\"box cta\" style=\"margin-top:1.5rem;\">
              <p class=\"has-text-centered\">
                  <span class=\"tag is-primary\">Nouveau</span> La première version du jeu est enfin disponible !
              </p>
  </div>
            <div class=\"columns features\" style=\"margin-top:1.5rem;\">
                <div class=\"column is-4\">
                    <div class=\"card is-shady\" style=\"padding:15px;\">
                        <div class=\"card-image has-text-centered\">
                            <i class=\"fa fa-user fa-5x\"></i>
                        </div>
                        <div class=\"card-content\">
                            <div class=\"content\">
                                <h4>Choisissez un personnage </h4>
                                <p>Chacun à des caractéristiques disctinctes, et par conséquent ses points forts et faiblesses... A vous de les exploiter au mieux</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=\"column is-4\">
                    <div class=\"card is-shady\" style=\"padding:15px;\">
                        <div class=\"card-image has-text-centered\">
                          <i class=\"fas fa-users fa-5x\"></i>
                        </div>
                        <div class=\"card-content\">
                            <div class=\"content\">
                                <h4>Combattez vos ennemis </h4>
                                <p>Combattez vos ennemis (et amis)! N'oubliez pas d'admirer le sympatique environnement pixélisé entre 2 coups d'épée !</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=\"column is-4\">
                    <div class=\"card is-shady\" style=\"padding:15px;\">
                        <div class=\"card-image has-text-centered\">
                            <i class=\"fas fa-trophy fa fa-5x\"></i>
                        </div>
                        <div class=\"card-content\">
                            <div class=\"content\">
                                <h4>Tentez de remporter la victoire </h4>
                                <p>Soyez le plus vifs pour dominer la partie ! Celui qui obtient le plus de points l'emporte ! Mais n'oubliez pas que ce n'est qu'un jeu...</p>
                            </div>
                        </div>
                    </div>
                </div>
</div>
</section>
<footer class=\"footer\" style=\"margin-top:1.5rem;\">
    <div class=\"container\">
        <div class=\"content has-text-centered\">
            <p>
                <a class=\"icon\" href=\"https://github.com/dansup/bulma-templates\">
                    <i class=\"fas fa-gamepad has-text-primary\"></i>
                </a>
            </p>
            <div class=\"control level-item\">
                <a href=\"https://github.com/dansup/bulma-templates\">
                    <div class=\"tags has-addons\">
                        <span class=\"tag is-dark\">Dev & Dungeon</span>
                        <span class=\"tag is-primary\">Damien Cortina</span>
                    </div>
                </a>
            </div>
        </div>
    </div>
</footer>
{% endblock %}

{% block javascripts %}

{{parent()}}

{% endblock %}
", "default/homepage.html.twig", "/var/www/megaman-3d/app/Resources/views/default/homepage.html.twig");
    }
}
