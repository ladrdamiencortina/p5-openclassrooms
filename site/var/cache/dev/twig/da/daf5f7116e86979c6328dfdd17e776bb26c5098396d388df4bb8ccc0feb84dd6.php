<?php

/* @FOSUser/layout.html.twig */
class __TwigTemplate_161ff00993450736558903760421a888ba39ae66b711fb7356c278f204cf21e7 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 4
        $this->parent = $this->loadTemplate("login.html.twig", "@FOSUser/layout.html.twig", 4);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "login.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/layout.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 7
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<style>

.stary{
  min-height: 100vh;
}

input[type=text], input[type=password] {
    background: transparent;
    outline : none;
    border : none;
    border-bottom : 2px solid white;
    border-radius : 0px;
    border-image : linear-gradient(to right,#007ec6 0%,#d71418 51%, #007ec6 100%) 1;
    color: white;
    font-weight: bold;
}
input[type=text]:focus, input[type=password]:focus {
    background : transparent;
    outline : none;
    border-bottom: 2px solid white;
    border-color:#007ec6;
    box-shadow: none;
    color:white;
}

input[type=text] + i, input[type=password] + i {
  color :white;
  transition: color 0.2s ease-in-out;
}

input[type=text]:focus + i, input[type=password]:focus + i {
  color : #007ec6;
}

.labelled-input {
  padding:0px 0px 0px 40px !important;
}

.btn-grad {
  position: relative;
  transition: background-color 0.4s ease-in-out;
  border-radius:25px;
  background-color: black;
  width:100%;
  height:50px;
  font-weight:bold;
  margin-top:30px;
  color:white;
}

.btn-grad:hover {
   background-color: transparent;
 }

 .btn-grad::after {
    position: absolute;
    top: -3px; bottom: -3px;
    left: -3px; right: -3px;
    background-image: linear-gradient(to right, #007ec6 0%,#d71418 51%, #007ec6 100%);
    content: '';
    z-index: -1;
    border-radius: 25px;
}

.btn-login{
  background-color: #0f161e;
}

form {
  padding : 0px 20px 50px 20px;
}

.login-logo {
  width:400px;
}

.field {
  margin-top:25px;
  position: relative;
}

.form-fa {
  height:0px;
  position: absolute;
  bottom : 30px;
  left:25px;
}

form{
  border:1px solid grey;
   border-radius:5px;
    background-color:rgba(0,0,0,0.3);
     color:white;
      z-index:100;
}

</style>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 109
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 110
        echo "<div class=\"stary\">
  <div id='stars'></div>
  <div id='stars2'></div>
  <div id='stars3'></div>
    <div class=\"container\" style=\"height:95vh;display:flex;align-items: center;justify-content: center; \">
      <div class=\"row\">
  ";
        // line 117
        echo "
  ";
        // line 118
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 120
        echo "</div>
</div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 118
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 119
        echo "  ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  208 => 119,  199 => 118,  186 => 120,  184 => 118,  181 => 117,  173 => 110,  164 => 109,  55 => 7,  46 => 6,  15 => 4,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# src/OC/UserBundle/Resources/views/layout.html.twig #}

{# On étend notre layout #}
{% extends \"login.html.twig\" %}

{% block stylesheets %}
{{parent()}}
<style>

.stary{
  min-height: 100vh;
}

input[type=text], input[type=password] {
    background: transparent;
    outline : none;
    border : none;
    border-bottom : 2px solid white;
    border-radius : 0px;
    border-image : linear-gradient(to right,#007ec6 0%,#d71418 51%, #007ec6 100%) 1;
    color: white;
    font-weight: bold;
}
input[type=text]:focus, input[type=password]:focus {
    background : transparent;
    outline : none;
    border-bottom: 2px solid white;
    border-color:#007ec6;
    box-shadow: none;
    color:white;
}

input[type=text] + i, input[type=password] + i {
  color :white;
  transition: color 0.2s ease-in-out;
}

input[type=text]:focus + i, input[type=password]:focus + i {
  color : #007ec6;
}

.labelled-input {
  padding:0px 0px 0px 40px !important;
}

.btn-grad {
  position: relative;
  transition: background-color 0.4s ease-in-out;
  border-radius:25px;
  background-color: black;
  width:100%;
  height:50px;
  font-weight:bold;
  margin-top:30px;
  color:white;
}

.btn-grad:hover {
   background-color: transparent;
 }

 .btn-grad::after {
    position: absolute;
    top: -3px; bottom: -3px;
    left: -3px; right: -3px;
    background-image: linear-gradient(to right, #007ec6 0%,#d71418 51%, #007ec6 100%);
    content: '';
    z-index: -1;
    border-radius: 25px;
}

.btn-login{
  background-color: #0f161e;
}

form {
  padding : 0px 20px 50px 20px;
}

.login-logo {
  width:400px;
}

.field {
  margin-top:25px;
  position: relative;
}

.form-fa {
  height:0px;
  position: absolute;
  bottom : 30px;
  left:25px;
}

form{
  border:1px solid grey;
   border-radius:5px;
    background-color:rgba(0,0,0,0.3);
     color:white;
      z-index:100;
}

</style>
{% endblock %}


{# Dans notre layout, il faut définir le block body #}
{% block body %}
<div class=\"stary\">
  <div id='stars'></div>
  <div id='stars2'></div>
  <div id='stars3'></div>
    <div class=\"container\" style=\"height:95vh;display:flex;align-items: center;justify-content: center; \">
      <div class=\"row\">
  {# On affiche les messages flash que définissent les contrôleurs du bundle #}

  {% block fos_user_content %}
  {% endblock fos_user_content %}
</div>
</div>
</div>
{% endblock %}
", "@FOSUser/layout.html.twig", "/var/www/megaman-3d/src/DC/UserBundle/Resources/views/layout.html.twig");
    }
}
