<?php

/* default/homepage.html.twig */
class __TwigTemplate_0eb98f769642f78b69c5347071315be8d5cf89beda7dcbec32c027fd56b71392 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 3
        $this->parent = $this->loadTemplate("base.html.twig", "default/homepage.html.twig", 3);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "  <div class=\"stary\">
    <div id='stars'></div>
    <div id='stars2'></div>
    <div id='stars3'></div>
  <div class=\"container\" style=\"height:100%; position: relative; z-index:0\">

<div class=\"row\" style=\"width:100%; align-items:center; position:absolute; top:30%;\">
  <div class=\"col-md-7 text-center\">
      <h1>Bienvenue sur Megaman3D</h1>
      <p>Ce jeu est un projet à but non lucratif, fais par un fan, pour les fans !</p>
    <a href=\"";
        // line 16
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("character_select");
        echo "\">
      <button class=\"btn btn-grad\">JOUER</button>
    </a>
</div>
<div class=\"col-md-5 text-center\">
  <img class=\"pixel\" src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/textures/player/MEGMF8F2.png"), "html", null, true);
        echo "\" style=\"width:100%; -webkit-transform: scaleX(-1); transform: scaleX(-1);\">
</div>

</div>
</div>
</div>

";
    }

    // line 30
    public function block_javascripts($context, array $blocks = array())
    {
        // line 31
        echo "
";
        // line 32
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "

";
    }

    public function getTemplateName()
    {
        return "default/homepage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 32,  71 => 31,  68 => 30,  56 => 21,  48 => 16,  36 => 6,  33 => 5,  15 => 3,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "default/homepage.html.twig", "/var/www/megaman-3d/app/Resources/views/default/homepage.html.twig");
    }
}
