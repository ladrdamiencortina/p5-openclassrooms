<?php

/* @FOSUser/layout.html.twig */
class __TwigTemplate_60be73a3dcd94b8f6d9f1eeba349f89010a81904605e0d005d3f9eed00ba6390 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 4
        $this->parent = $this->loadTemplate("login.html.twig", "@FOSUser/layout.html.twig", 4);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "login.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 7
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<style>
input[type=text], input[type=password] {
    background: transparent;
    outline : none;
    border : none;
    border-bottom : 2px solid white;
    border-radius : 0px;
    border-image : linear-gradient(to right,#007ec6 0%,#d71418 51%, #007ec6 100%) 1;
    color: white;
    font-weight: bold;
}
input[type=text]:focus, input[type=password]:focus {
    background : transparent;
    outline : none;
    border-bottom: 2px solid white;
    border-color:#007ec6;
    box-shadow: none;
    color:white;
}

input[type=text] + i, input[type=password] + i {
  color :white;
  transition: color 0.2s ease-in-out;
}

input[type=text]:focus + i, input[type=password]:focus + i {
  color : #007ec6;
}

.labelled-input {
  padding:0px 0px 0px 40px !important;
}

.btn-grad {
  position: relative;
  transition: background-color 0.4s ease-in-out;
  border-radius:25px;
  background-color: black;
  width:100%;
  height:50px;
  font-weight:bold;
  margin-top:30px;
  color:white;
}

.btn-grad:hover {
   background-color: transparent;
 }

 .btn-grad::after {
    position: absolute;
    top: -3px; bottom: -3px;
    left: -3px; right: -3px;
    background-image: linear-gradient(to right, #007ec6 0%,#d71418 51%, #007ec6 100%);
    content: '';
    z-index: -1;
    border-radius: 25px;
}

.btn-login{
  background-color: #0f161e;
}

form {
  padding : 0px 20px 50px 20px;
}

.login-logo {
  width:400px;
}

.field {
  margin-top:25px;
  position: relative;
}

.form-fa {
  height:0px;
  position: absolute;
  bottom : 30px;
  left:25px;
}

form{
  border:1px solid grey;
   border-radius:5px;
    background-color:rgba(0,0,0,0.3);
     color:white;
      z-index:100;
}

</style>
";
    }

    // line 104
    public function block_body($context, array $blocks = array())
    {
        // line 105
        echo "<div class=\"stary\">
  <div id='stars'></div>
  <div id='stars2'></div>
  <div id='stars3'></div>
  <img src=\"";
        // line 109
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/img/megaman_login _emptied.png"), "html", null, true);
        echo "\" style=\"position:absolute; width:100%; height:100vh;\">
    <div class=\"container\" style=\"height:95vh;display:flex;align-items: center;justify-content: center; \">
      <div class=\"row\">
  ";
        // line 113
        echo "
  ";
        // line 114
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 116
        echo "</div>
</div>
</div>
";
    }

    // line 114
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 115
        echo "  ";
    }

    public function getTemplateName()
    {
        return "@FOSUser/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  165 => 115,  162 => 114,  155 => 116,  153 => 114,  150 => 113,  144 => 109,  138 => 105,  135 => 104,  37 => 7,  34 => 6,  15 => 4,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/layout.html.twig", "/var/www/megaman-3d/src/DC/UserBundle/Resources/views/layout.html.twig");
    }
}
