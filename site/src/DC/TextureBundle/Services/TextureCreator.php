<?php

namespace DC\TextureBundle\Services;

use DC\TextureBundle\Entity\Texture;

class TextureCreator
{

    //--------------------------------------------------------------------------
    //creates a bordered rectangular face
    //--------------------------------------------------------------------------

    /*
     *name : name of the created face
     *origin : name of the origin file
     *width : width of the createface (must be a multiple of the origin size)
     *height : height of the createface (must be a multiple of the origin size)
     */

    private function createBorderedFace($name, $origin, $width, $height)
    {
      $baseFace = imagecreatetruecolor($width*32,$height*32);
      imagepng($baseFace,"../web/assets/textures/created/".$name.".png");
      $face =imagecreatefrompng("../web/assets/textures/created/".$name.".png");
      $base=imagecreatefrompng("../web/assets/textures/origin/".$origin.".png");
      $posX= 0;
      $posY= 0;
      while($posY<$height*32){
        while($posX<$width*32){
          if($posY===0){
            if($posX===0){
              imagecopymerge($face,$base,$posX,$posY,0,0,32,32,100);
            }
            elseif($posX===($width*32-32)){
              imagecopymerge($face,$base,$posX,$posY,96,0,32,32,100);
            }
            else{
              imagecopymerge($face,$base,$posX,$posY,32,0,32,32,100);
            }
          }elseif($posY===($height*32-32)){
            if($posX===0){
              imagecopymerge($face,$base,$posX,$posY,0,96,32,32,100);
            }
            elseif($posX===($width*32-32)){
              imagecopymerge($face,$base,$posX,$posY,96,96,32,32,100);
            }
            else{
              imagecopymerge($face,$base,$posX,$posY,32,96,32,32,100);
            }
          }elseif($posX===0){
            imagecopymerge($face,$base,$posX,$posY,0,32,32,32,100);
          }elseif($posX===$width*32-32){
            imagecopymerge($face,$base,$posX,$posY,96,32,32,32,100);
          }else{
            imagecopymerge($face,$base,$posX,$posY,32,32,32,32,100);
          }
          $posX=$posX+32;
        }
        $posX=0;
        $posY=$posY+32;
      }
      return $face;
    }



    //--------------------------------------------------------------------------
    //creates a randomly textured rectangular face from a set of different textures
    //--------------------------------------------------------------------------

    /*
     *texture : texture entity containing file name and textures coordinates
     *name : name of the created face
     *width : width of the createface
     *height : height of the createface
     */

    private function createFace(Texture $texture, String $name, $width, $height)
    {
      //create a new image
      $face = imagecreatetruecolor($width*$texture->getSize()->getWidth(),$height*$texture->getSize()->getHeight());
      $base = imagecreatefrompng(__DIR__."/../../../../web/assets/textures/origin/".$texture->getFile());
      //array containing every tiles that'll be used to generate the random texture
      $tiles = unserialize($texture->getCoordinates());
      $posX= 0;
      $posY= 0;
      while($posY<$height*$texture->getSize()->getHeight()){
        while($posX<$width*$texture->getSize()->getWidth()){
          //if the texture contains more than one tile, a random one is selected each time
          if(count($tiles)>1){
            $tile = array_rand($tiles,1);
          }else{
            $tile = 0;
          }
          imagecopy($face,$base,$posX,$posY,$tiles[$tile][0]*$texture->getSize()->getWidth(),$tiles[$tile][1]*$texture->getSize()->getHeight(),$texture->getSize()->getWidth(),$texture->getSize()->getHeight());
          $posX=$posX+$texture->getSize()->getWidth();
        }
        $posX=0;
        $posY=$posY+$texture->getSize()->getHeight();
      }
      //created face must be saved in its parent function (some images may not need to be saved as they may be rotated later)
      return $face;
    }



    //--------------------------------------------------------------------------
    //creates a full texture file for a parallelepipedic mesh
    //--------------------------------------------------------------------------

    /*
     *name : name of the created texture
     *floorTexture : texture entity containing file name and textures coordinates that'll be used for the floor
     *wallTexture : texture entity containing file name and textures coordinates that'll be used for walls
     *ceilingTexture : texture entity containing file name and textures coordinates that'll be used for the ceiling
     *width : width of the mesh
     *height : height of the mesh
     *depth : depth of the mesh
     */

    public function createMeshTexture(String $name, Texture $floorTexture, Texture $wallTexture, Texture $ceilingTexture, $width, $height, $depth)
    {
      if($floorTexture->getSize() == $wallTexture->getSize() && $wallTexture->getSize() == $ceilingTexture->getSize()){
        $textureWidth=$width*4*$floorTexture->getSize()->getWidth() + $height*2*$floorTexture->getSize()->getHeight();
        if($depth<$height){
          $textureHeight=$height*$floorTexture->getSize()->getHeight();
        }else{
          $textureHeight=$depth*$floorTexture->getSize()->getWidth();
        }
        $baseImage = imagecreatetruecolor($textureWidth,$textureHeight);
        imagepng($baseImage,__DIR__."/../../../../web/assets/textures/created/".$name.".png");
        $textureMap = imagecreatefrompng(__DIR__."/../../../../web/assets/textures/created/".$name.".png");
        //back face
        $posX = 0;
        $back = $this->createFace($wallTexture,"back",$width,$height);
        imagecopy($textureMap, $back, $posX,0,0,0,$width*$floorTexture->getSize()->getWidth(),$height*$floorTexture->getSize()->getHeight());
        //front face
        $posX = $posX+$width*$floorTexture->getSize()->getWidth();
        $front = $this->createFace($wallTexture,"front",$width,$height);
        $front = imagerotate($front,180,0);
        imagecopy($textureMap, $front, $posX,0,0,0,$width*$floorTexture->getSize()->getWidth(),$height*$floorTexture->getSize()->getHeight());
        //first side
        $posX = $posX+$width*$floorTexture->getSize()->getWidth();
        $side1 = $this->createFace($wallTexture,"side1",$depth,$height);
        $side1 = imagerotate($side1,90,0);
        imagecopy($textureMap, $side1, $posX,0,0,0,$height*$floorTexture->getSize()->getWidth(),$depth*$floorTexture->getSize()->getWidth());
        //second side
        $posX = $posX+$height*$floorTexture->getSize()->getHeight();
        $side2 = $this->createFace($wallTexture,"side2",$depth,$height);
        $side2 = imagerotate($side2,90,0);
        imagecopy($textureMap, $side2, $posX,0,0,0,$height*$floorTexture->getSize()->getWidth(),$depth*$floorTexture->getSize()->getWidth());
        //top face
        $posX = $posX+$height*$floorTexture->getSize()->getHeight();
        $top = $this->createFace($ceilingTexture,"top",$width,$depth);
        imagecopy($textureMap, $top, $posX,0,0,0,$width*$floorTexture->getSize()->getWidth(),$depth*$floorTexture->getSize()->getHeight());
        //bottom face
        $posX = $posX+$width*$floorTexture->getSize()->getWidth();
        $bottom = $this->createFace($floorTexture,"bottom",$width,$depth);
        imagecopy($textureMap, $bottom, $posX,0,0,0,$width*$floorTexture->getSize()->getWidth(),$depth*$floorTexture->getSize()->getHeight());
        //saving created texture
        imagepng($textureMap,__DIR__."/../../../../web/assets/textures/created/".$name.".png");
        return true;
      }else{
        return false;
      }
    }

}
