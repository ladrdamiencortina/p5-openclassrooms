<?php

namespace DC\TextureBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use DC\TextureBundle\Entity\Texture;

class TextureController extends Controller
{

    private $textureCreator;

    /**
     * @Route("/texture")
     */
    public function indexAction()
    {
      $textureCreator = $this->get('dc_texture.texturecreator');
      $em = $this->getDoctrine()->getManager();
      $wallTexture = $em->getRepository(Texture::class)->find(5);
      $floorTexture = $em->getRepository(Texture::class)->find(5);
      $ceilingTexture = $em->getRepository(Texture::class)->find(5);
      $textureCreator->createMeshTexture('base_ne_test', $floorTexture, $wallTexture,  $ceilingTexture,1,1,1);
      return $this->render('DCTextureBundle:Default:index.html.twig');
    }

}
