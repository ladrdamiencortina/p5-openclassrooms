Player = function(game, canvas, character){

  var _this = this;
  this.character = character;
  this.game = game;
  this.angularSensibility = 200;
  this.speed = this.character.speed;
  this.isAlive = true;
  this.spriteDead = null;
  this.weponShoot = false;
  this.axisMovement = [false,false,false,false];

  window.addEventListener("mousemove", function(evt) {
    if(_this.rotEngaged === true){
        _this.camera.playerBox.rotation.y+=evt.movementX * 0.001 * (_this.angularSensibility / 250);
        var nextRotationX = _this.camera.playerBox.rotation.x + (evt.movementY * 0.001 * (_this.angularSensibility / 250));
        if( nextRotationX < degToRad(90) && nextRotationX > degToRad(-75)){
            _this.camera.playerBox.rotation.x+=evt.movementY * 0.001 * (_this.angularSensibility / 250);
        }
        var data={
            rotation : _this.camera.playerBox.rotation
        };
        _this.sendNewData(data)
    }
  }, false);

  window.addEventListener("keyup", function(evt) {
      if(evt.keyCode == 90 || evt.keyCode == 83 || evt.keyCode == 81 || evt.keyCode == 68 ) {
          switch (evt.keyCode) {
              case 90:
                  _this.camera.axisMovement[0] = false;
                  break;
              case 83:
                  _this.camera.axisMovement[1] = false;
                  break;
              case 81:
                  _this.camera.axisMovement[2] = false;
                  break;
              case 68:
                  _this.camera.axisMovement[3] = false;
                  break;
          }
          var data = {
              axisMovement: _this.camera.axisMovement
          };
          _this.sendNewData(data)
      }
  }, false);

  window.addEventListener("keydown", function(evt) {
      if(evt.keyCode == 90 || evt.keyCode == 83 || evt.keyCode == 81 || evt.keyCode == 68 ){
    switch(evt.keyCode){
        case 90:
        _this.camera.axisMovement[0] = true;
        break;
        case 83:
        _this.camera.axisMovement[1] = true;
        break;
        case 81:
        _this.camera.axisMovement[2] = true;
        break;
        case 68:
        _this.camera.axisMovement[3] = true;
        break;
    }
          var data={
              axisMovement : _this.camera.axisMovement
          };
          _this.sendNewData(data)
      }
  }, false);

  var canvas = this.game.scene.getEngine().getRenderingCanvas();

  canvas.addEventListener("click", function(evt) {
    if(_this.camera.weapons.canFire){
      _this.camera.weapons.canFire = false;
      _this.handleUserMouseDown();
      var data = {
          attack: true
      };
      _this.sendNewData(data)
    }
  }, false);

  this._initCamera(this.game.scene, canvas);

};

Player.prototype = {

  _initPointerLock : function() {
      var _this = this;

      var canvas = this.game.scene.getEngine().getRenderingCanvas();

      canvas.addEventListener("click", function(evt) {
          canvas.requestPointerLock = canvas.requestPointerLock || canvas.msRequestPointerLock || canvas.mozRequestPointerLock || canvas.webkitRequestPointerLock;
          if (canvas.requestPointerLock) {
              canvas.requestPointerLock();
          }
      }, false);

      var pointerlockchange = function (event) {
          _this.controlEnabled = (document.mozPointerLockElement === canvas || document.webkitPointerLockElement === canvas || document.msPointerLockElement === canvas || document.pointerLockElement === canvas);
          if (!_this.controlEnabled) {
              _this.rotEngaged = false;
          } else {
              _this.rotEngaged = true;
          }
      };

      document.addEventListener("pointerlockchange", pointerlockchange, false);
      document.addEventListener("mspointerlockchange", pointerlockchange, false);
      document.addEventListener("mozpointerlockchange", pointerlockchange, false);
      document.addEventListener("webkitpointerlockchange", pointerlockchange, false);
  },

_initCamera : function(scene, canvas,) {

    let randomPoint = Math.random();
    randomPoint = Math.round(randomPoint * (this.game.allSpawnPoints.length - 1));
    this.spawnPoint = this.game.allSpawnPoints[randomPoint];

    var playerBox = BABYLON.Mesh.CreateBox("headMainPlayer", 3, scene);
    playerBox.visibility = 0;
    playerBox.position = this.spawnPoint.clone();
    playerBox.ellipsoid = new BABYLON.Vector3(10, 16 , 10);

    this.camera = new BABYLON.FreeCamera("camera", new BABYLON.Vector3(0, 0, -1), scene);
    this.camera.playerBox = playerBox;
    this.camera.parent = this.camera.playerBox;

    this.camera.health = 300;

    // L'armure du joueur
    this.camera.armor = 0;

    // On réinitialise la position de la caméra
    this.camera.setTarget(BABYLON.Vector3.Zero());
    this.game.scene.activeCamera = this.camera;

    // Ajout des collisions avec playerBox
    this.camera.playerBox.checkCollision = true;
    this.camera.playerBox.applyFravity = true;

    // Pour savoir si c'est le joueur princpal
    this.camera.isMain = true;

    // Axe de mouvement X et Z
    this.camera.axisMovement = [false, false, false, false];

    // Si le joueur est en vie ou non
    this.isAlive = true;

    // On demande à la caméra de regarder au point zéro de la scène
    this.camera.setTarget(BABYLON.Vector3.Zero());

    // Le joueur doit cliquer dans la scène pour que controlEnabled soit changé
    this.controlEnabled = false;

    // On lance l'event _initPointerLock pour checker le clic dans la scène
    this._initPointerLock();

    console.log(this.character.defaultWeapon);
    //Appel de la création des armes
    this.camera.weapons =  new Weapons(this,this.character.defaultWeapon);

    var hitBoxPlayer = BABYLON.Mesh.CreateBox("hitBoxPlayer", 20, scene);
    hitBoxPlayer.parent = this.camera.playerBox;
    // hitBoxPlayer.scaling.y = 2;
    hitBoxPlayer.visibility = 0;
    hitBoxPlayer.isPickable = true;
    hitBoxPlayer.isMain = true;

    this.ghostPlayers=[];
},

    _checkMove : function(ratioFps){

        // On bouge le player en lui attribuant la caméra

        this._checkUniqueMove(ratioFps,this.camera);

        for (var i = 0; i < this.ghostPlayers.length; i++) {

            // On bouge chaque ghost présent dans ghostPlayers

            this._checkUniqueMove(ratioFps,this.ghostPlayers[i]);

        }

    },

_checkUniqueMove : function(ratioFps, player) {
    let relativeSpeed = this.speed / ratioFps;
    var playerSelected = player;
    if (playerSelected.axisMovement) {
        if (playerSelected.head) {
            var rotationPoint = playerSelected.head.rotation;
        } else {
            var rotationPoint = playerSelected.playerBox.rotation;
        }
        if (playerSelected.axisMovement[0]) {
            forward = new BABYLON.Vector3(
                parseFloat(Math.sin(parseFloat(rotationPoint.y))) * relativeSpeed,
                0,
                parseFloat(Math.cos(parseFloat(rotationPoint.y))) * relativeSpeed
            );
            playerSelected.playerBox.moveWithCollisions(forward);
        }
        if (playerSelected.axisMovement[1]) {
            backward = new BABYLON.Vector3(
                parseFloat(-Math.sin(parseFloat(rotationPoint.y))) * relativeSpeed,
                0,
                parseFloat(-Math.cos(parseFloat(rotationPoint.y))) * relativeSpeed
            );
            playerSelected.playerBox.moveWithCollisions(backward);
        }
        if (playerSelected.axisMovement[2]) {
            left = new BABYLON.Vector3(
                parseFloat(Math.sin(parseFloat(rotationPoint.y) + degToRad(-90))) * relativeSpeed,
                0,
                parseFloat(Math.cos(parseFloat(rotationPoint.y) + degToRad(-90))) * relativeSpeed
            );
            playerSelected.playerBox.moveWithCollisions(left);
        }
        if (playerSelected.axisMovement[3]) {
            right = new BABYLON.Vector3(
                parseFloat(-Math.sin(parseFloat(rotationPoint.y) + degToRad(-90))) * relativeSpeed,
                0,
                parseFloat(-Math.cos(parseFloat(rotationPoint.y) + degToRad(-90))) * relativeSpeed
            );
            playerSelected.playerBox.moveWithCollisions(right);
        }
        this.camera.playerBox.moveWithCollisions(new BABYLON.Vector3(0, -(0, 001 * relativeSpeed), 0));
    }
},


handleUserMouseDown : function() {
    if(this.isAlive === true){
        this.camera.weapons.fire();
        this.camera.weapons.meleeAttack();
    }
},

    getDamage : function(damage,whoDammage){
        var damageTaken = damage;
        // Tampon des dégâts par l'armure
        if(this.camera.armor > Math.round(damageTaken/2)){
            this.camera.armor -= Math.round(damageTaken/2);
            damageTaken = Math.round(damageTaken/2);
        }else{
            damageTaken = damageTaken - this.camera.armor;
            this.camera.armor = 0;
        }

        // Si le joueur i a encore de la vie
        if(this.camera.health>damageTaken){
            this.camera.health-=damageTaken;
        }else{
            // Sinon, il est mort
            this.playerDead(whoDammage);
        }
    },


    playerDead : function(whoKilled) {
        sendPostMortem(whoKilled);
        this.deadCamera = new BABYLON.ArcRotateCamera("ArcRotateCamera",
            1, 0.8, 10, new BABYLON.Vector3(
                this.camera.playerBox.position.x,
                this.camera.playerBox.position.y,
                this.camera.playerBox.position.z),
            this.game.scene);

        this.game.scene.activeCamera = this.deadCamera;
        this.deadCamera.attachControl(this.game.scene.getEngine().getRenderingCanvas());
        // Suppression de la playerBox
        this.camera.playerBox.dispose();

// Suppression de la camera
        this.camera.dispose();

// Suppression de l'arme
        this.camera.weapons.mesh.dispose();

// On signale à Weapons que le joueur est mort
        this.isAlive=false;
        var newPlayer = this;
        var canvas = this.game.scene.getEngine().getRenderingCanvas();
        setTimeout(function(){
            newPlayer._initCamera(newPlayer.game.scene, canvas, newPlayer.spawnPoint);
            newPlayer.launchRessurection();
        }, 4000);
    },

    launchRessurection : function(){
        ressurectMe();
    },


    sendNewData : function(data){
        // this._updateGhostSideTexture();
        updateGhost(data);
    },

    sendActualData : function(){
        return {
            actualTypeWeapon : this.camera.weapons.actualWeapon,
            armor : this.camera.armor,
            life : this.camera.health,
            position  : this.camera.playerBox.position,
            rotation : this.camera.playerBox.rotation,
            axisMovement : this.camera.axisMovement
        }
    },

    updateLocalGhost : function(data){

        ghostPlayers = this.ghostPlayers;

        for (var i = 0; i < ghostPlayers.length; i++) {
            if(ghostPlayers[i].idRoom === data.id){
                var boxModified = ghostPlayers[i].playerBox;
                if(data.position){
                    boxModified.position = new BABYLON.Vector3(data.position.x,data.position.y,data.position.z);
                }
                if(data.axisMovement){
                    ghostPlayers[i].axisMovement = data.axisMovement;
                    ghostPlayers[i].sprite.position = ghostPlayers[i].playerBox.position;
                    if(!ghostPlayers[i].isAttacking){
                      if(data.axisMovement[0] || data.axisMovement[1] || data.axisMovement[2] || data.axisMovement[3]){
                        if(!ghostPlayers[i].isMoving){
                          ghostPlayers[i].isMoving = true;
                          ghostPlayers[i].sprite.playAnimation(ghostPlayers[i].walk[0],ghostPlayers[i].walk[1],true,200);
                        }
                      }else{
                        if(ghostPlayers[i].isMoving){
                          ghostPlayers[i].isMoving = false;
                          ghostPlayers[i].sprite.stopAnimation();
                          ghostPlayers[i].sprite.cellIndex = ghostPlayers[i].still;
                        }
                      }
                  }else{
                    ghostPlayers[i].isMoving = false;
                  }
                }
                if(data.attack){
                  ghostPlayers[i].isAttacking = true;
                  var isMoving = ghostPlayers[i].isMoving;
                  var ghostPlayer = ghostPlayers[i];
                  ghostPlayers[i].sprite.playAnimation(ghostPlayers[i].attack[0],ghostPlayers[i].attack[1],false,200,function(i){
                    this.cellIndex = ghostPlayer.still;
                    ghostPlayer.isAttacking = false;
                    if(ghostPlayer.isMoving){
                      ghostPlayer.sprite.playAnimation(ghostPlayer.walk[0],ghostPlayer.walk[1],true,200);
                    }
                  });
                }
                if(data.rotation){
                     ghostPlayers[i].head.rotation.y = data.rotation.y;
                }
            }

        }
    }

};
