GhostPlayer = function(game,ghostData,idRoom) {

    this.game = game;

    var fakePlayer = {};

    fakePlayer.isMoving = false;
    fakePlayer.isAttacking = false;
    fakePlayer.still = ghostData.spriteStill;
    fakePlayer.walk = ghostData.spriteWalk;
    fakePlayer.attack = ghostData.spriteAttack;
    fakePlayer.hurt = ghostData.spriteHurt;
    fakePlayer.dead = ghostData.spriteDead;

    var positionSpawn = new BABYLON.Vector3(ghostData.position.x,
        ghostData.position.y,
        ghostData.position.z);

    var rotationSpawn = new BABYLON.Vector3(ghostData.rotation.x,
        ghostData.rotation.y,
        ghostData.rotation.z);

    fakePlayer.playerBox = BABYLON.Mesh.CreateBox(ghostData.id, 30, this.game.scene);
    fakePlayer.playerBox.position = positionSpawn;
    fakePlayer.playerBox.isPlayer = true;
    fakePlayer.playerBox.isPickable = true;

    fakePlayer.playerBox.material = new BABYLON.StandardMaterial("textureGhost", this.game.scene);
    fakePlayer.playerBox.material.alpha = 0;

    fakePlayer.playerBox.checkCollisions = true;
    fakePlayer.playerBox.applyGravity = true;
    fakePlayer.playerBox.ellipsoid = new BABYLON.Vector3(1.5, 1, 1.5);

    fakePlayer.head = BABYLON.Mesh.CreateBox('headGhost', 2.2, this.game.scene);
    fakePlayer.head.parent = fakePlayer.playerBox;
    fakePlayer.head.scaling = new BABYLON.Vector3(2,0.8,2)
    fakePlayer.head.position.y+=1.6;
    fakePlayer.head.isPickable = false;

    fakePlayer.bodyChar = BABYLON.Mesh.CreateBox('bodyGhost', 2.2, this.game.scene);
    fakePlayer.bodyChar.parent = fakePlayer.playerBox;
    fakePlayer.bodyChar.scaling = new BABYLON.Vector3(2,0.8,2)
    fakePlayer.bodyChar.position.y-=0.6;
    fakePlayer.bodyChar.isPickable = false;

    var spriteManagerPlayer = new BABYLON.SpriteManager("ghostManager","../../assets/textures/player/characters.png", 1, {width: 32, height: 32}, this.game.scene,0.01,1);
    sprite = new BABYLON.Sprite("ghost", spriteManagerPlayer);
    sprite.size = 32;
    sprite.position = fakePlayer.playerBox.position
    sprite.cellIndex = ghostData.spriteStill;
    fakePlayer.sprite = sprite;



    // Les datas de vie et d'armure du joueur

    fakePlayer.health = ghostData.life;

    fakePlayer.armor  = ghostData.armor;


// La place du joueur dans le tableau des joueurs, gérée par le serveur

    fakePlayer.idRoom = idRoom;


// L'axe de mouvement. C'est lui qui recevra les informations de touches pressées envoyées par le joueur

    fakePlayer.axisMovement = ghostData.axisMovement;


// Le nom réel du joueur

    fakePlayer.namePlayer = ghostData.name;


// A nouveau l'id du joueur

    fakePlayer.uniqueId = ghostData.uniqueId;


// La rotation. Comme pour le mouvement, elle sert à déterminer le sens de déplacement

    fakePlayer.rotation = rotationSpawn;


// Les materials qui définissent la couleur du joueur

    fakePlayer.head.material = new BABYLON.StandardMaterial("textureGhost", this.game.scene);
    fakePlayer.head.material.alpha = 0;

    fakePlayer.bodyChar.material = new BABYLON.StandardMaterial("textureGhost", this.game.scene);
    fakePlayer.bodyChar.material.alpha = 0;

    deleteGameGhost = function(game,deletedIndex){

        ghostPlayers = game._PlayerData.ghostPlayers;

        for (var i = 0; i < ghostPlayers.length; i++) {

            if(ghostPlayers[i].idRoom === deletedIndex){

                ghostPlayers[i].playerBox.dispose();

                ghostPlayers[i].sprite.playAnimation(ghostPlayers[i].dead[0],ghostPlayers[i].dead[1],false,200,function(){
                  this.dispose();
                })

                ghostPlayers[i].head.dispose();

                ghostPlayers[i].bodyChar.dispose();

                ghostPlayers[i] = false;

                ghostPlayers.splice(i,1);

                break;

            }



        }

    }


    return fakePlayer;



}
