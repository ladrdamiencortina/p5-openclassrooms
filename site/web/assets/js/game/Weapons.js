Weapons = function(Player,weaponId){

  var that = this;

  this.id = weaponId;
  this.Player = Player;
  this.mesh = null;
  this.name = null;
  this.dammage = null;
  this.range = null;
  this.fireRate = null;
  this.image = null;
  this.weaponFrameSpeed = null;
  this._deltaFireRate = null;
  this.canFire = true;
  this.attack = false;

  this.init(Player);

  var engine = Player.game.scene.getEngine();

  Player.game.scene.registerBeforeRender(function() {
    if (!that.canFire) {
        that._deltaFireRate -= engine.getDeltaTime();
        if (that._deltaFireRate <= 0  && that.Player.isAlive) {
            that.canFire = true;
            that._deltaFireRate = that.fireRate;
        }
    }
  });
};

Weapons.prototype = {

  init : function(Player){
    var httpRequest = new XMLHttpRequest();
    if (!httpRequest) {
        console.log('Impossible de créer la requète');
    }
    var that = this;
    httpRequest.onreadystatechange = function(){
      try{
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
          if (httpRequest.status === 200) {
            var response = JSON.parse(httpRequest.responseText);
            that.name = response.name;
            that.dammage = response.dammage;
            that.range = response.range;
            that.fireRate = response.fireRate;
            that.weaponFrameSpeed = response.fireRate/4;
            that._deltaFireRate = response.fireRate;
            that.image = response.image;
            that.mesh = that.newWeapon(Player);
            var animationBox = new BABYLON.Animation("meleeAttackAnimation", "position", 30, BABYLON.Animation.ANIMATIONTYPE_VECTOR3, BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE);
            var keys = [];
            keys.push({
                frame: 0,
                value: new BABYLON.Vector3(10,-5,15)
            });
            keys.push({
                frame: 10,
                value: new BABYLON.Vector3(12,5,17)
            });
            keys.push({
                frame: 15,
                value: new BABYLON.Vector3(5,-10,32)
            });
            keys.push({
                frame: 30,
                value: new BABYLON.Vector3(10,-5,15)
            });
            animationBox.setKeys(keys);
            var eventHit = new BABYLON.AnimationEvent(15, function() {
              console.log(that.Player.ghostPlayers);
                for (var i = 0; i < that.Player.ghostPlayers.length; i++) {
                  if(that.Player.camera.weapons.mesh.intersectsMesh(that.Player.ghostPlayers[i].playerBox)){
                    console.log(that.Player.ghostPlayers[i].playerBox);
                    console.log(that.Player.camera.weapons.dammage);
                    sendDamages(that.Player.camera.weapons.dammage,that.Player.ghostPlayers[i].playerBox.name);
                    var ghostPlayer = ghostPlayers[i];
                    that.Player.ghostPlayers[i].sprite.playAnimation(ghostPlayers[i].hurt[0],ghostPlayers[i].hurt[1],false,200,function(i){
                      this.cellIndex = ghostPlayer.still;
                      if(ghostPlayer.isMoving){
                        ghostPlayer.sprite.playAnimation(ghostPlayer.walk[0],ghostPlayer.walk[1],true,200);
                      }
                    });
                  }
              }
            }, true);
            animationBox.addEvent(eventHit);

            that.mesh.animations.push(animationBox);
          } else {
            alert('Erreur dans la requète.');
          }
        }
      }
      catch( error ) {
        alert('Erreur : ' + error.description);
      }
    };
    httpRequest.open('GET', 'armory/'+this.id, true);
    httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    httpRequest.send();
  },


  newWeapon : function(Player){
      var newWeapon = BABYLON.Mesh.CreatePlane('weapon', 15, Player.game.scene);

      // On l'associe à la caméra pour qu'il bouge de la même facon
      newWeapon.parent = Player.camera;
      newWeapon.position.x = 10;
      newWeapon.position.y = -5;
      newWeapon.position.z = 15;

      var materialWeapon = new BABYLON.StandardMaterial('buster', Player.game.scene);
      materialWeapon.diffuseTexture = new BABYLON.Texture("../../assets/textures/player/"+this.image,Player.game.scene);
      materialWeapon.diffuseTexture.hasAlpha = true;

      newWeapon.material = materialWeapon;

      return newWeapon
  },

  fire : function(pickInfo) {
    this.attack = true;
  },

meleeAttack : function() {
var that = this;
var animatable = this.Player.game.scene.beginAnimation(this.mesh, 0, 30, false);
animatable.onAnimationEnd = function(){
  that.attack = false;
}
}

};
